package info.mallmc.framework.events;

import info.mallmc.core.api.GameState;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.core.database.PlayerData;
import info.mallmc.core.database.ServerData;
import info.mallmc.core.util.TextUtils;
import info.mallmc.framework.Framework;
import info.mallmc.framework.interactions.Interaction;
import info.mallmc.framework.interactions.InteractionRegistry;
import info.mallmc.framework.util.ChatUtils;
import info.mallmc.framework.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerEvents implements Listener {

  @EventHandler
  public void onMove(PlayerMoveEvent event) {
    if (MallPlayer.getPlayer(event.getPlayer().getUniqueId()).isFrozen()) {
      event.setTo(event.getPlayer().getLocation());
    }
    InteractionRegistry.handleEvent(Interaction.MOVE, event);
  }

  @EventHandler
  public void onLogin(PlayerLoginEvent event) {
    final Player player = event.getPlayer();
    if (Framework.getInstance().isDoneLoading()) {
      if (Framework.getInstance().getCurrentServer().isWhitelisted()) {
        PermissionSet playerPermissionSet = PlayerData.getInstance().getPlayerByUUID(player.getUniqueId()).getPermissionSet();
        if (playerPermissionSet.getPower() < Framework.getInstance().getCurrentServer().getWhitelistLevel().getPower()) {
          event.setResult(Result.KICK_WHITELIST);
          String message = Messaging.getMessage(PlayerData.getInstance().getPlayerByUUID(player.getUniqueId()).getLanguageIdentifier(), "global.whitelist.kick");
          message = TextUtils.replace(message, Framework.getInstance().getCurrentServer().getWhitelistLevel().getName());
          event.setKickMessage(Messaging.colorizeMessage(message));
          if (Framework.getInstance().getCurrentServer().getOverride().contains(player.getUniqueId())) {
            event.setResult(Result.ALLOWED);
          }
        }
      }
    } else {
      event.setResult(Result.KICK_OTHER);
      event.setKickMessage(Messaging.getMessage(PlayerData.getInstance().getPlayerByUUID(player.getUniqueId()).getLanguageIdentifier(), "framework.server.stillloading"));
    }
    if (event.getResult() != Result.ALLOWED) {
      event.disallow(event.getResult(), event.getKickMessage());
    } else {
      MallPlayer.createPlayer(event.getPlayer().getUniqueId(), event.getPlayer().getName());
    }
  }

  @EventHandler
  public void onQuit(PlayerQuitEvent event) {
    final Player player = event.getPlayer();
    event.setQuitMessage(null);
    Messaging.logmessage(MallPlayer.getPlayer(player.getUniqueId()), false);
    player.getInventory().clear();
    if (Framework.getInstance().getCurrentServer().getGameState() == GameState.LOBBY || Framework.getInstance().getCurrentServer().getGameState() == GameState.IN_GAME) {
      ServerData.getInstance().saveServer(Framework.getInstance().getCurrentServer());
    }
    MallPlayer.removePlayer(player.getUniqueId());
  }

  @EventHandler
  public void onJoin(PlayerJoinEvent event) {
    event.setJoinMessage(null);
    final Player player = event.getPlayer();
    final MallPlayer mp = MallPlayer.getPlayer(player.getUniqueId());
    Messaging.logmessage(mp, true);
    player.setFoodLevel(20);
    if (Framework.getInstance().getCurrentServer().getGameState() != GameState.LOBBY) {
      //TODO: TabList
    }
    if (Framework.getInstance().getCurrentServer().getGameState() == GameState.LOBBY || Framework.getInstance().getCurrentServer().getGameState() == GameState.IN_GAME) {
      ServerData.getInstance().saveServer(Framework.getInstance().getCurrentServer());
    }
    if (mp.getPermissionSet().getPower() >= PermissionSet.SNR_STAFF.getPower()) {
      if (!player.isOp()) {
        player.setOp(true);
      }
    }
  }

  @EventHandler
  public void onPlayerInteract(PlayerInteractEvent event) {
    switch (event.getAction()) {
      case RIGHT_CLICK_AIR:
        InteractionRegistry.handleEvent(Interaction.RIGHT_CLICK, event);
        InteractionRegistry.handleEvent(Interaction.RIGHT_CLICK_AIR, event);
        break;
      case RIGHT_CLICK_BLOCK:
        InteractionRegistry.handleEvent(Interaction.RIGHT_CLICK, event);
        InteractionRegistry.handleEvent(Interaction.RIGHT_CLICK_BLOCK, event);
        break;
      case LEFT_CLICK_AIR:
        InteractionRegistry.handleEvent(Interaction.LEFT_CLICK, event);
        InteractionRegistry.handleEvent(Interaction.LEFT_CLICK_AIR, event);
        break;
      case LEFT_CLICK_BLOCK:
        InteractionRegistry.handleEvent(Interaction.LEFT_CLICK, event);
        InteractionRegistry.handleEvent(Interaction.LEFT_CLICK_BLOCK, event);
        break;
    }
  }

  @EventHandler
  public void onTabComplete(PlayerChatTabCompleteEvent event) {
    if (event.getChatMessage().contains("@") && event.getChatMessage().length() != 0) {
      if (event.getChatMessage().length() == 1) {
        for (Player p : Bukkit.getOnlinePlayers()) {
          event.getTabCompletions().add("@" + p.getName());
        }
      } else {
        for (Player p : Bukkit.getOnlinePlayers()) {
          if (p.getName().startsWith(event.getChatMessage().split("@")[1])) {
            event.getTabCompletions().add("@" + p.getName());
          }
        }
      }
    }
  }

  @EventHandler
  public void onPlayerChat(AsyncPlayerChatEvent event) {
    event.setCancelled(true);
    final Player player = event.getPlayer();
    MallPlayer mallPlayer = MallPlayer.getPlayer(player.getUniqueId());
    if (!ChatUtils.hasProfanity(mallPlayer, event.getMessage())) {
      if (ChatUtils.isInCaps(event.getMessage())) {
        char[] msg = event.getMessage().toLowerCase().toCharArray();
        msg[0] = Character.toUpperCase(msg[0]);
        event.setMessage(new String(msg));
      }
      ChatUtils.isMentioned(mallPlayer, event.getMessage());
      if (event.getMessage().startsWith("!") && event.getMessage().length() > 1) {
        event.setMessage(ChatColor.BOLD + event.getMessage().replaceFirst("!", ""));
      }
      if (event.getMessage().startsWith("**") && event.getMessage().length() > 1) {
        event.setMessage(Messaging.colorizeMessage(event.getMessage().replaceFirst("\\*\\*", "")));
      }
      ChatUtils.sendMessage(mallPlayer, event.getMessage());
    }
  }


  @EventHandler
  public void playerInteractEntity(PlayerInteractEntityEvent event) {
    InteractionRegistry.handleEvent(Interaction.RIGHT_CLICK_ENTITY, event);
    InteractionRegistry.handleEvent(Interaction.INTERACT_ENTITY, event);
  }

  @EventHandler
  public void inventoryClickEvent(InventoryClickEvent event) {
    InteractionRegistry.handleEvent(Interaction.CLICK_INSIDE_INVENTORY, event);
  }

  @EventHandler
  public void damageEvent(EntityDamageByEntityEvent event) {
    InteractionRegistry.handleEvent(Interaction.LEFT_CLICK_ENTITY, event);
  }

  @EventHandler
  public void inventoryClose(InventoryCloseEvent event){
    InteractionRegistry.handleEvent(Interaction.CLOSE_INVENTORY, event);
  }

  @EventHandler
  public void inventoryDrag(InventoryDragEvent event){
    InteractionRegistry.handleEvent(Interaction.DRAG_INVENTORY, event);
  }
}
