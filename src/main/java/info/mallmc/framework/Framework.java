package info.mallmc.framework;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.GameState;
import info.mallmc.core.api.Server;
import info.mallmc.core.api.logs.LL;
import info.mallmc.core.api.logs.Log;
import info.mallmc.core.database.ServerData;
import info.mallmc.framework.commands.admin.MutePlayerCommand;
import info.mallmc.framework.commands.admin.SetCurrencyCommand;
import info.mallmc.framework.commands.admin.WhitelistCommand;
import info.mallmc.framework.commands.util.DebugCommand;
import info.mallmc.framework.commands.util.LetterCommand;
import info.mallmc.framework.commands.util.PictureFrameCommand;
import info.mallmc.framework.commands.util.ReloadMessagesCommand;
import info.mallmc.framework.events.PlayerEvents;
import info.mallmc.framework.events.custom.FrameworkEnableEvent;
import info.mallmc.framework.events.custom.OneMinuteEvent;
import info.mallmc.framework.events.custom.OneSecondEvent;
import info.mallmc.framework.interactions.FrameworkInteractions;
import info.mallmc.framework.interactions.InteractionRegistry;
import info.mallmc.framework.util.MallCommand;
import info.mallmc.framework.util.SimpleScoreboard;
import info.mallmc.framework.util.XPManager;
import java.io.File;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.configuration.Configuration;
import org.bukkit.craftbukkit.v1_12_R1.CraftServer;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.mongodb.morphia.mapping.DefaultCreator;

public class Framework extends JavaPlugin {

  private static Framework instance;

  public static Framework getInstance(){
    return instance;
  }

  private MallCore mallCore;
  private Configuration configuration;
  private CommandMap commandMap;
  private Server server;
  public Logger logger;
  private XPManager xpManager;
  private boolean doneLoading;

  @Override
  public void onEnable() {
    instance = this;
    PluginManager pluginManager = Bukkit.getPluginManager();
    logger = Bukkit.getLogger();
    xpManager = new XPManager();
    xpManager.initializeLevels();
    createConfig();
    String ip = getConfig().getString("database.ip");
    int port = getConfig().getInt("database.port");
    String username = getConfig().getString("database.username");
    String password = getConfig().getString("database.password");
    String redisIP = getConfig().getString("redis.ip");
    int redisPort = getConfig().getInt("redis.port");
    String redisPassword = getConfig().getString("redis.password");
    this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
    InetAddress IP;
    try {
      IP = InetAddress.getLocalHost();
    } catch (UnknownHostException e) {
      e.printStackTrace();
      return;
    }
    this.mallCore = new MallCore(IP.getHostAddress(), Bukkit.getPort());
    this.mallCore.connect(ip, port, username, password);
    this.mallCore.redisConnect(redisIP, redisPassword, redisPort);
    this.mallCore.getDatabase().getMorphia().getMapper().getOptions().setObjectFactory(new DefaultCreator() {
      @Override
      protected ClassLoader getClassLoaderForClass() {
        return this.getClass().getClassLoader();
      }
    });
    this.mallCore.getDatabase().setupMorphia();
    this.mallCore.getDatabase().setupLanguage();
    SimpleScoreboard.getManager().start();
    GameState.setGameState(GameState.LOBBY);
    pluginManager.registerEvents(new PlayerEvents(), this);
    FrameworkInteractions.registerInteractions();
    if(getServer() instanceof CraftServer){
      Field f;
      try {
        f = CraftServer.class.getDeclaredField("commandMap");
        f.setAccessible(true);
        commandMap = (CommandMap) f.get(getServer());
      }catch (Exception ex){
        Log.error(LL.ERROR, server.getNickname(), "LOADING","Command Map", ex.getMessage());
        logger.info(ex.getMessage());
      }
    }
    MallCommand.registerCommand("framework", "setcurrency", new SetCurrencyCommand());
    MallCommand.registerCommand("framework", "mute", new MutePlayerCommand());
    MallCommand.registerCommand("framework", "debug", new DebugCommand());
    MallCommand.registerCommand("framework", "whitelist", new WhitelistCommand());
    MallCommand.registerCommand("framework", "letter", new LetterCommand());
    MallCommand.registerCommand("framework", "frames", new PictureFrameCommand());
    MallCommand.registerCommand("framework", "langreload", new ReloadMessagesCommand());
//    xpManager = new XPManager();
//    xpManager.initializeLevels();
    Bukkit.getScheduler().scheduleSyncDelayedTask(this, () -> {
      FrameworkEnableEvent event = new FrameworkEnableEvent();
      Bukkit.getServer().getPluginManager().callEvent(event);
      server = this.mallCore.getServer();
      this.doneLoading = true;
      logger.info("Hello! I am " + server.getNickname() + " and i am ready!");
    }, 20L);

    Bukkit.getScheduler()
        .scheduleSyncRepeatingTask(
            this,
            () -> {
              OneSecondEvent e = new OneSecondEvent();
              Bukkit.getServer().getPluginManager().callEvent(e);
            }, 20L, 20L);

    Bukkit.getScheduler()
        .scheduleSyncRepeatingTask(
            this,
            () -> {
              OneMinuteEvent e = new OneMinuteEvent();
              Bukkit.getServer().getPluginManager().callEvent(e);
              ServerData.getInstance().saveServer(getCurrentServer());
            }, 20L, 60 * 20L);
  }

  @Override
  public void onDisable() {
    mallCore.disable();
    InteractionRegistry.clear();
  }

  public boolean isDoneLoading() {
    return doneLoading;
  }

  public Server getCurrentServer() {
    return server;
  }

  public CommandMap getCommandMap(){
    return commandMap;
  }

  public XPManager getXpManager() {
    return xpManager;
  }

  private void createConfig() {
    try {
      if (!getDataFolder().exists()) {
        getDataFolder().mkdirs();
      }
      File file = new File(getDataFolder(), "config.yml");
      if (!file.exists()) {
        logger.info("Config.yml not found, creating it now!");
        saveDefaultConfig();
      } else {
        logger.info("Config.yml found, loading");
      }
    } catch (Exception e) {
      logger.info(e.getMessage());
    }
  }

}
