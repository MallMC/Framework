package info.mallmc.framework.entities;

import java.lang.reflect.Field;
import net.minecraft.server.v1_12_R1.Entity;
import net.minecraft.server.v1_12_R1.MinecraftKey;
import org.bukkit.Location;

public enum EntityTypes {
    HUB_VILLAGER("HubVillager", 120, VillagerNPC.class);
    private String name;
    private int id;
    private Class<? extends Entity> custom;

    EntityTypes(String name, int id, Class<? extends Entity> custom) {
        this.name = name;
        this.id = id;
        this.custom = custom;
        addToMaps(custom, name, id);
    }

    public static Entity spawnEntity(Entity entity, Location loc, String name) {
        if (entity instanceof VillagerNPC) {
            VillagerNPC villagerNPC = (VillagerNPC) entity;
            return villagerNPC.spawn(loc, name);
        }
        return null;
    }

    private static void addToMaps(Class clazz, String name, int id) {
        net.minecraft.server.v1_12_R1.EntityTypes.b.a(id, new MinecraftKey(name), clazz);
    }

    public static Object getPrivateField(String fieldName, Class clazz, Object object) {
        Field field;
        Object o = null;
        try {
            field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);
            o = field.get(object);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return o;
    }
}
