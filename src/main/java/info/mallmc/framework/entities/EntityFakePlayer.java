package info.mallmc.framework.entities;

import com.mojang.authlib.GameProfile;
import info.mallmc.core.api.logs.LL;
import info.mallmc.core.api.logs.Log;
import info.mallmc.framework.Framework;
import net.minecraft.server.v1_12_R1.EntityPlayer;
import net.minecraft.server.v1_12_R1.MinecraftServer;
import net.minecraft.server.v1_12_R1.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_12_R1.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_12_R1.PlayerConnection;
import net.minecraft.server.v1_12_R1.PlayerInteractManager;
import net.minecraft.server.v1_12_R1.WorldServer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class EntityFakePlayer {

  public static EntityFakePlayer deserializeNPC(String serializedNPC) {
    String splits[] = serializedNPC.split(";");
    if (splits[6].equals(ProfileManager.getMallSecurity().getId().toString())) {
      return NPCManager.getMallSecurity();
    }
    return null;
  }

  private MinecraftServer nmsServer;
  private WorldServer nmsWorld;
  private GameProfile gameProfile;

  public EntityFakePlayer(MinecraftServer nmsServer, WorldServer nmsWorld,
      GameProfile gameProfile) {
    this.nmsServer = nmsServer;
    this.gameProfile = gameProfile;
    this.nmsWorld = nmsWorld;
  }

  public void create(Player p, Location location) {
    final EntityPlayer npc = new EntityPlayer(nmsServer, nmsWorld, gameProfile,
        new PlayerInteractManager(nmsWorld));
    npc.setPositionRotation(location.getX(), location.getY(), location.getZ(), location.getYaw(),
        location.getPitch());
    final PlayerConnection connection = ((CraftPlayer) p).getHandle().playerConnection;

    connection.sendPacket(
        new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, npc));
    connection.sendPacket(new PacketPlayOutNamedEntitySpawn(npc));
    Bukkit.getScheduler().runTaskLater(Framework.getInstance(), () -> connection.sendPacket(
        new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER,
            npc)), 200L);
  }

  public String serializeNPC(Location location) {
    if (gameProfile == null) {
      Log.error(LL.ERROR, Framework.getInstance().getCurrentServer().getNickname(),
          "GameProfile serialising", "Game Profile Null", "GameProfile is null HELP ME!");
      return null;
    }
    if (gameProfile.getId() == null) {
      Log.error(LL.ERROR, Framework.getInstance().getCurrentServer().getNickname(),
          "GameProfile serialising", "Game Profile UUID Null", "GameProfile is uuid null HELP ME!");
      return null;
    }
    return location.getX() + ";" + location.getY() + ";" + location.getZ() + ";" + location.getYaw()
        + ";" + location.getPitch() + ";" + location.getWorld().getUID() + ";" + gameProfile.getId()
        .toString();
  }

  public void setGameProfile(GameProfile profile) {
    this.gameProfile = profile;
  }
}
