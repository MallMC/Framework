package info.mallmc.framework.util;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.logs.LL;
import info.mallmc.core.api.logs.Log;
import info.mallmc.core.api.player.EnumLanguageIdentifier;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.util.TextUtils;
import info.mallmc.framework.Framework;
import java.util.Arrays;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.minecraft.server.v1_12_R1.ChatMessageType;
import net.minecraft.server.v1_12_R1.IChatBaseComponent;
import net.minecraft.server.v1_12_R1.PacketPlayOutChat;
import net.minecraft.server.v1_12_R1.PacketPlayOutTitle;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.libs.jline.internal.Nullable;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class Messaging {

  public final static EnumLanguageIdentifier COMMAND_SENDER_LANGUAGE_IDENTIFER = EnumLanguageIdentifier.EN;


  public static String getPrefix(EnumLanguageIdentifier languageIdentifer) {
    return colorizeMessage(getMessage(languageIdentifer, "global.prefix"));
  }

  /**
   * @param key The name of the message
   * @param replacements Things to replace in the message
   * @return The message
   */
  public static String getMessage(EnumLanguageIdentifier languageIdentifier, String key, Object... replacements) {
    String message = MallCore.getInstance().getLanguageMap().get(languageIdentifier)
        .getMessage(key);
    if (message == null) {
      Log.error(LL.ERROR, Framework.getInstance().getCurrentServer().getNickname(), "Message",
          "Key is null", key + " could not be found");
    }
    return TextUtils.replace(message, replacements);

  }

  /**
   * @return Colorized message
   */
  public static String colorizeMessage(String message) {
    if (message == null || message.isEmpty()) {
      return "";
    }
    return ChatColor.translateAlternateColorCodes('&', message);
  }

  /**
   * @param commandSender consol
   * @param key The predefined message key
   * @param replacements The replacements for the message
   */
  public static void sendMessage(CommandSender commandSender, String key,
      Object... replacements) {
    if(commandSender instanceof  Player){
      MallPlayer mallPlayer = MallPlayer.getPlayer(((Player) commandSender).getUniqueId());
      commandSender.sendMessage(colorizeMessage(getPrefix(mallPlayer.getLanguageIdentifier()) + getMessage(mallPlayer.getLanguageIdentifier(), key, replacements)));
    }else{
      commandSender.sendMessage(colorizeMessage(getPrefix(COMMAND_SENDER_LANGUAGE_IDENTIFER) + getMessage(COMMAND_SENDER_LANGUAGE_IDENTIFER, key, replacements)));
    }
  }

  /**
   * Sends a predefined message to a Player
   *
   * @param player The player to send it to
   * @param key The predefined message key
   * @param replacements The replacements for the message
   */
  public static void sendMessage(MallPlayer player, String key, Object... replacements) {
    Bukkit.getPlayer(player.getUuid()).sendMessage(colorizeMessage(
        getPrefix(player.getLanguageIdentifier()) + getMessage(player.getLanguageIdentifier(), key,
            replacements)));
  }

  /**
   * Broadcasts a predefined message to the server
   *
   * @param key Predefined message key
   * @param replacements The replacements for the message
   */
  public static void broadcastMessage(String key, Object... replacements) {
    for (MallPlayer mallPlayer : MallPlayer.getPlayers().values()) {
      sendMessage(mallPlayer, key, replacements);
    }
    sendMessage(Bukkit.getConsoleSender(), key, replacements);
  }


  /**
   * @param commandSender consol
   * @param key The predefined message key
   * @param replacements The replacements for the message
   */
  public static void sendRawMessage(CommandSender commandSender, String key,
      Object... replacements) {
    if(commandSender instanceof  Player){
      MallPlayer mallPlayer = MallPlayer.getPlayer(((Player) commandSender).getUniqueId());
      commandSender.sendMessage(
          colorizeMessage(getMessage(mallPlayer.getLanguageIdentifier(), key, replacements)));
    }else{
      commandSender.sendMessage(
          colorizeMessage(getMessage(COMMAND_SENDER_LANGUAGE_IDENTIFER, key, replacements)));
    }
  }

  /**
   * @param commandSender consol
   * @param key The predefined message key
   * @param replacements The replacements for the message
   */
  public static void sendRawTitleMessage(CommandSender commandSender, String key,
      Object... replacements) {
    if(commandSender instanceof  Player){
      MallPlayer mallPlayer = MallPlayer.getPlayer(((Player) commandSender).getUniqueId());
      commandSender.sendMessage(TextUtils.getCenteredMessage(colorizeMessage(getMessage(mallPlayer.getLanguageIdentifier(), key, replacements))));
    }else{
      commandSender.sendMessage(TextUtils.getCenteredMessage(colorizeMessage(getMessage(COMMAND_SENDER_LANGUAGE_IDENTIFER, key, replacements))));
    }
  }





  /**
   * Sends a predefined message to a Player
   *
   * @param player The player to send it to
   * @param key The predefined message key
   * @param replacements The replacements for the message
   */
  public static void sendRawMessage(MallPlayer player, String key, Object... replacements) {
    Bukkit.getPlayer(player.getUuid()).sendMessage(colorizeMessage(getMessage(player.getLanguageIdentifier(), key,
            replacements)));
  }
  /**
   * Sends a predefined message to a Player
   *
   * @param player The player to send it to
   * @param key The predefined message key
   * @param replacements The replacements for the message
   */
  public static void sendRawTitleMessage(MallPlayer player, String key, Object... replacements) {
    Bukkit.getPlayer(player.getUuid()).sendMessage(TextUtils.getCenteredMessage(colorizeMessage(getMessage(player.getLanguageIdentifier(), key,
        replacements))));
  }

  /**
   * Broadcasts a predefined message to the server
   *
   * @param key Predefined message key
   * @param replacements The replacements for the message
   */
  public static void broadcastRawMessage(String key, Object... replacements) {
    for (MallPlayer mallPlayer : MallPlayer.getPlayers().values()) {
      sendRawMessage(mallPlayer, key, replacements);
    }
    sendRawMessage(Bukkit.getConsoleSender(), key, replacements);
  }


  /**
   * Broadcasts a predefined message to the server
   *
   * @param key Predefined message key
   * @param replacements The replacements for the message
   */
  public static void broadcastTitleRawMessage(String key, Object... replacements) {
    for (MallPlayer mallPlayer : MallPlayer.getPlayers().values()) {
      sendRawTitleMessage(mallPlayer, key, replacements);
    }
    sendRawTitleMessage(Bukkit.getConsoleSender(), key, replacements);
  }


  /**
   * Send the join/leave message for a player
   *
   * @param mallPlayer Name of player
   * @param login Is it login or logout?
   */
  public static void logmessage(MallPlayer mallPlayer, boolean login) {
    if (login) {
      broadcastRawMessage("framework.playerJoin", mallPlayer.getUsername(),
          mallPlayer.getRank().getColor());
      return;
    }
    broadcastRawMessage("framework.playerLogout", mallPlayer.getUsername(),
        mallPlayer.getRank().getColor());
  }

    /* Other Messaegs */

  /**
   * Sends the player a hovering message
   *
   * @param mallPlayer   The player to send it to
   * @param message      The message to send
   * @param hover        The hovering message
   * @param replacements The replacements for the message
   */
  public static void sendHoveringMessage(MallPlayer mallPlayer, String message, String hover,  Object... replacements) {
    TextComponent messageComponent = new TextComponent(TextComponent.fromLegacyText(TextUtils.replace(getPrefix(mallPlayer.getLanguageIdentifier()) + message, replacements)));
    messageComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Arrays.toString(TextComponent.fromLegacyText(TextUtils.replace(message, replacements)))).create()));
    Bukkit.getPlayer(mallPlayer.getUuid()).spigot().sendMessage(messageComponent);
  }


  /**
   * Send the player a clickable and hoverable message
   *
   * @param mallPlayer       The player to send it to
   * @param message      The message to send
   * @param hover        The hover message
   * @param command      the command to run on click
   * @param replacements The replacements for the message
   */
  public static void sendClickableAndHoveringMessage(MallPlayer mallPlayer, String message, String hover,
      String command, Object... replacements) {
    TextComponent messageComponent = new TextComponent(
        TextComponent.fromLegacyText(getPrefix(mallPlayer.getLanguageIdentifier()) + TextUtils.replace(message, replacements)));
    messageComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));
    messageComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(
        Arrays.toString(TextComponent.fromLegacyText(TextUtils.replace(message, replacements)))).create()));
    Bukkit.getPlayer(mallPlayer.getUuid()).spigot().sendMessage(messageComponent);
  }

  /**
   * Sends the player a raw(no prefix) hovering message
   *
   * @param player       The player to send it to
   * @param message      The message to send
   * @param hover        The hovering message
   * @param replacements The replacements for the message
   */
  public static void sendRawHoveringMessage(Player player, String message, String hover,
      Object... replacements) {
    TextComponent messageComponent = new TextComponent(
        TextComponent.fromLegacyText(TextUtils.replace(message, replacements)));
    messageComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(
        Arrays.toString(TextComponent.fromLegacyText(TextUtils.replace(message, replacements)))).create()));
    player.spigot().sendMessage(messageComponent);
  }

  /**
   * Send the player a raw(No Prefix) clickable message
   *
   * @param player       The player to send it to
   * @param message      The message to send
   * @param command      The command to execute upon clicking it
   * @param replacements The replacements for the message
   */
  public static void sendRawClickableMessage(Player player, String message, String command,
      Object... replacements) {
    TextComponent messageComponent = new TextComponent(
        TextComponent.fromLegacyText(TextUtils.replace(message, replacements)));
    messageComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));
    player.spigot().sendMessage(messageComponent);
  }

  /**
   * Send the player a raw(No Prefix) clickable and hoverable message
   *
   * @param player       The player to send it to
   * @param message      The message to send
   * @param hover        The hover message
   * @param command      the command to run on click
   * @param replacements The replacements for the message
   */
  public static void sendRawClickableAndHoveringMessage(Player player, String message, String hover,
      String command, Object... replacements) {
    TextComponent messageComponent = new TextComponent(
        TextComponent.fromLegacyText(TextUtils.replace(message, replacements)));
    messageComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));
    messageComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(
        Arrays.toString(TextComponent.fromLegacyText(TextUtils.replace(message, replacements)))).create()));
    player.spigot().sendMessage(messageComponent);
  }


  public static void sendPlayerActionbar(MallPlayer mallPlayer, String t, Object... replacements) {
    String text = Messaging.colorizeMessage(Messaging.getMessage(mallPlayer.getLanguageIdentifier(), t, replacements));
    IChatBaseComponent icbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + text + "\"}");
    PacketPlayOutChat ppoc = new PacketPlayOutChat(icbc, ChatMessageType.GAME_INFO);
    ((CraftPlayer) Bukkit.getPlayer(mallPlayer.getUuid())).getHandle().playerConnection.sendPacket(ppoc);
  }

  public static void sendTitleOrSubtitle(MallPlayer player, PacketPlayOutTitle.EnumTitleAction titleType, int fadeInTime, int showTime, int fadeOutTime, String text, Object... replacements) {
    IChatBaseComponent chatTitle = IChatBaseComponent.ChatSerializer
        .a("{\"text\": \"" + Messaging.colorizeMessage(Messaging.getMessage(player.getLanguageIdentifier(), text, replacements)) + "\", \"color\": \""
            .toLowerCase() + "\"}");

    if (titleType != PacketPlayOutTitle.EnumTitleAction.TITLE
        && titleType != PacketPlayOutTitle.EnumTitleAction.SUBTITLE) {
      return;
    }
    PacketPlayOutTitle title = new PacketPlayOutTitle(titleType, chatTitle);
    PacketPlayOutTitle length = new PacketPlayOutTitle(fadeInTime, showTime, fadeOutTime);

    Player player1 = Bukkit.getPlayer(player.getUuid());
    ((CraftPlayer) player1).getHandle().playerConnection.sendPacket(title);
    ((CraftPlayer) player1).getHandle().playerConnection.sendPacket(length);
  }





  //Todo Make This A Better Way (Was Used In The Restart Command Only)
//  public static void countdown(String action, boolean fast, ChatColor... colors) {
//    ChatColor color1 = ChatColor.GOLD;
//    ChatColor color2 = ChatColor.AQUA;
//    try {
//      if (colors != null && colors.length == 2) {
//        color1 = colors[0];
//        color2 = colors[1];
//      }
//    } catch (NullPointerException e) {
//      Log.error(LL.ERROR, Servers.getServerData().getName(), "Countdown", "Colors is null", " See countdown method for more infomation");
//    }
//    if (GameState.gameTimer < 0) {
//      return;
//    }
//    if (GameState.gameTimer == 0) {
//      for (Player ps : Bukkit.getOnlinePlayers()) {
//        ps.playSound(ps.getLocation(), Sound.BLOCK_NOTE_PLING, 1, 0);
//      }
//      return;
//    }
//    if (GameState.gameTimer == 1 && fast) {
//      Messaging.broadcastCustomMessage(
//          color1 + action + " in " + color2 + GameState.gameTimer + color1 + " second!");
//    } else if (GameState.gameTimer % 10 == 0 || GameState.gameTimer == 5
//        || fast && GameState.gameTimer < 5 && GameState.gameTimer > 1) {
//      Messaging.broadcastCustomMessage(
//          color1 + action + " in " + color2 + GameState.gameTimer + color1 + " seconds!");
//    }
//    if (GameState.gameTimer <= 10 && fast) {
//      for (Player ps : Bukkit.getOnlinePlayers()) {
//        ps.playSound(ps.getLocation(), Sound.BLOCK_NOTE_PLING, 1, 0);
//      }
//    }
//  }
}
