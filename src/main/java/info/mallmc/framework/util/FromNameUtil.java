package info.mallmc.framework.util;

import info.mallmc.core.api.player.Rank;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;

public class FromNameUtil {


    public static Material getMaterialFromName(String name, Material error, int id) {
        Material material = error;
        try {
            material = Material.getMaterial(name.toUpperCase());
            if (material == null) {
                throw new NullPointerException();
            }
        } catch (Exception e) {
            material = Material.DEAD_BUSH;
        } finally {
            return material;
        }
    }

    public static Material getMaterialFromName(String name, Material error) {
        Material material = error;
        try {
            material = Material.getMaterial(name.toUpperCase());
            if (material == null) {
                throw new NullPointerException();
            }
        } catch (Exception e) {
            material = Material.DEAD_BUSH;
        } finally {
            return material;
        }
    }



//    public static ParticleEffect getParticleEffectFromName(String name, ParticleEffect error) {
//        ParticleEffect rank = error;
//        try {
//            rank = ParticleEffect.valueOf(name);
//            if (rank == null) {
//                throw new NullPointerException();
//            }
//        } catch (Exception e) {
//            rank = error;
//        } finally {
//            return rank;
//        }
//    }

    public static Enchantment getEnchantmentFromName(String name, Enchantment error) {
        Enchantment enchantment = null;
        try {
            if (name == null) {
                return null;
            }
            enchantment = Enchantment.getByName(name);
            if (enchantment == null) {
                throw new NullPointerException();
            }
        } catch (Exception e) {
            enchantment = error;
        } finally {
            return enchantment;
        }
    }
}
