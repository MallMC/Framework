package info.mallmc.framework.util;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.util.items.ItemUtil;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class InventoryItems
{
    public static ItemStack getCloseInventoryItem(MallPlayer mallPlayer) {
      return ItemUtil.createItem(Material.BARRIER, Messaging
              .getMessage(mallPlayer.getLanguageIdentifier(), "global.inventory.item.close.name"),
          Messaging
              .getMessage(mallPlayer.getLanguageIdentifier(), "global.inventory.item.close.lore"));
    }
}
