package info.mallmc.framework.util;

import info.mallmc.core.api.player.MallPlayer;
import java.util.ArrayList;

public class XPManager {


  private int intitalexp;
  private int starterlevel;

  private int baseCurve;
  private int difficulty;

  private int maximumlevelCapacity;

  private ArrayList<Integer> levelcurve = new ArrayList<>();


  public XPManager() {
    intitalexp = 0;
    starterlevel = 0;
    baseCurve = 30;
    difficulty = 5;
    maximumlevelCapacity = 101;
  }


  /**
   * Grab the amount of exp till the next level
   * </br>
   * This function is designed for API usage.
   *
   * @param player The player required amount of exp till next level.
   */
  public float getneededXP(MallPlayer player) {
    int level = player.getLevel();

    if (player.getLevel() == maximumlevelCapacity - 1) {
      return 0;
    } else {
      float formula = levelcurve.get(level + 1) - player.getXp();

      return formula;
    }
  }

  /**
   * Level Manager
   */
  /**
   * Generates the level Stricture!
   * </br>
   * This function is designed for API usage.
   */
  public void initializeLevels() {
    for (int i = starterlevel; i < maximumlevelCapacity - 1; i++) {

      int formula = i * difficulty;

      levelcurve.add(baseCurve * formula);
    }
  }


  /**
   * Retrieve the specific player's level!
   * </br>
   * This function is designed for API usage.
   *
   * @param player The player you're wanting to grab the levels from.
   */

  public int getLevel(MallPlayer player) {
    float exp = player.getXp();

    int returnValue = starterlevel;

    for (int i = starterlevel; i < maximumlevelCapacity - 1; i++) {

      int formula = (maximumlevelCapacity - 1) * difficulty;

      if (player.getXp() >= baseCurve * formula) {

//                player.setXp(baseCurve * formula);

        return maximumlevelCapacity - 1;
      }
      if (exp < levelcurve.get(i) && exp >= levelcurve.get(i - 1)) {
        returnValue = (i - 1);
        return returnValue;
      }

    }

    return returnValue;

  }

  /**
   * Set a player to a specific level
   * </br>
   * This function is designed for API usage.
   *
   * @param player The player you're wanting to grab the levels from.
   * @param level The level you would like to set the player to!
   */

  public void setLevel(MallPlayer player, int level) {
    float exp = player.getXp();
    float needed = getneededXP(player);

    if (getLevel(player) == maximumlevelCapacity - 1) {
      // Do whatever
    } else {

//            player.setXp(needed * level);

    }
  }

  public int getXPForLevel(int level) {
    return levelcurve.get(level);
  }


}