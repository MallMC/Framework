package info.mallmc.framework.util.jukebox;

import info.mallmc.framework.util.jukebox.MallMedia;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import net.mcjukebox.plugin.bukkit.MCJukebox;
import net.mcjukebox.plugin.bukkit.api.JukeboxAPI;
import net.mcjukebox.plugin.bukkit.managers.shows.Show;
import net.mcjukebox.plugin.bukkit.utils.MessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.json.JSONObject;

public class MallJukebox {

  private final List<UUID> players;
  private final String name;

  public MallJukebox(String name)
  {
    this.players = new ArrayList<>();
    this.name = name;
    getShow();
  }

  public static void playSoundToPlayer(Player player, MallMedia mallMedia) {
    JukeboxAPI.play(player, mallMedia);
  }

  public static void stopSoundForPlayer(Player player) {
    JukeboxAPI.stopMusic(player);
  }
  public static void playSoundToAllPlayers(MallMedia mallMedia) {
    for (Player player : Bukkit.getOnlinePlayers()) {
      playSoundToPlayer(player, mallMedia);
    }
  }
  public static void stopSoundForAllPlayers()
  {
    for(Player player: Bukkit.getOnlinePlayers()) {
      stopSoundForPlayer(player);
    }
  }
  public static void sendPlayerLink(Player player) {
    MessageUtils.sendMessage(player, "user.openLoading");
    JSONObject params = new JSONObject();
    params.put("username", player.getName());
    MCJukebox.getInstance().getSocketHandler().emit("command/getToken", params);
  }
  public void addPlayer(Player player)
  {
    players.add(player.getUniqueId());
    getShow().addMember(player, false);
  }
  public void removePlayer(Player player) {
    if (players.contains(player.getUniqueId())) {
      players.remove(player.getUniqueId());
    }
    getShow().removeMember(player);
  }
  public void playSoundForShow(MallMedia mallMedia)
  {
    getShow().play(mallMedia);
  }

  private Show getShow()
  {
    return JukeboxAPI.getShowManager().getShow(name);
  }
}
