package info.mallmc.framework.util.jukebox;

import net.mcjukebox.plugin.bukkit.api.ResourceType;
import net.mcjukebox.plugin.bukkit.api.models.Media;

public class MallMedia extends Media {

  public MallMedia(String URL) {
    super(ResourceType.SOUND_EFFECT, URL);
  }
  public MallMedia(String URL, boolean music) {
    super(ResourceType.MUSIC, URL);
  }
}
