package info.mallmc.framework.util;

import info.mallmc.core.api.logs.LL;
import info.mallmc.core.api.logs.Log;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.framework.Framework;
import info.mallmc.framework.api.IMallExecutor;
import java.util.List;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

public class MallCommand extends Command {

  private CommandExecutor exe = null;
  private TabCompleter completer = null;
  private PermissionSet needPermissionSet = null;
  private SenderType senderType = null;
  private String commandName = null;

  public MallCommand(String name) {
    super(name);
  }

  public static void registerCommand(String prefix, String label, IMallExecutor exe) {
    MallCommand command = new MallCommand(label);
    Framework.getInstance().getCommandMap().register(prefix, command);
    command.setExecutor(exe);
    command.setTabCompleter(exe);
    command.setNeedPermissionSet(exe.getNeedPermissionSet());
    command.setSenderType(exe.getSenderType());
    command.setCommandName(prefix + ":" + label);
  }

  @Override
  public boolean execute(CommandSender sender, String commandLabel, String[] args) {
    if (exe != null) {
      if (needPermissionSet != null) {
        if (senderType != null) {
            if(sender instanceof ConsoleCommandSender)
            {
              ConsoleCommandSender consoleCommandSender = (ConsoleCommandSender) sender;
              if(senderType == SenderType.BOTH || senderType == SenderType.CONSOLE)
              {
                exe.onCommand(sender, this, commandLabel, args);
                return true;
              }else
              {
                Messaging.sendMessage(consoleCommandSender, "global.command.playerNeeded");
                return false;
              }
            }else if (sender instanceof Player) {
              Player player = (Player) sender;
              MallPlayer mallPlayer = MallPlayer.getPlayer(player.getUniqueId());
              if(mallPlayer.getPermissionSet().getPower() >= needPermissionSet.getPower())
              {
                if(senderType == SenderType.BOTH || senderType == SenderType.PLAYER)
                {
                  exe.onCommand(sender, this, commandLabel, args);
                  return true;
                }else
                {
                  //Sends Msg to player that they have to be console to send this msg
                  Messaging.sendMessage(mallPlayer, "global.command.consoleNeeded");
                 return false;
                }
              }else {
                //Todo Add replace part With Rank Power Don't Know how to do that yet in a good way?
                Messaging.sendMessage(mallPlayer, "global.permissions.notAllowed");
                return false;
              }
            }else
            {
              //Todo Is This the way we want it?
              //Returns False As We Don't want commands to be run form command blocks and stuff?
              return false;
            }
        } else {
          Log.error(LL.ERROR, Framework.getInstance().getCurrentServer().getNickname(), "Command Processing", "SenderType IS NULL", "SenderType is null, this can not be null pls fix it command is " + commandName);
          return false;
        }
      } else {
        Log.error(LL.ERROR, Framework.getInstance().getCurrentServer().getNickname(), "Command Processing", "PermissionSet IS NULL", "PermissionSet is null, this can not be null pls fix it command is " + commandName);
        return false;
      }
    } else {
      Log.error(LL.ERROR, Framework.getInstance().getCurrentServer().getNickname(), "Command Processing", "CommandExecutor IS NULL", "CommandExecutor is null, this is not good happened while registering command " + commandName);
      return false;
    }
  }

  @Override
  public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
    if (completer != null) {
      if (sender instanceof ConsoleCommandSender) {
        if (senderType == SenderType.BOTH || senderType == SenderType.CONSOLE) {
          return completer.onTabComplete(sender, this, alias, args);
        } else {
          return null;
        }
      } else if (sender instanceof Player) {
        if (senderType == SenderType.BOTH || senderType == SenderType.PLAYER) {
          Player playerTabCompleter = (Player) sender;
          MallPlayer mallPlayerTabCompleter = MallPlayer
              .getPlayer(playerTabCompleter.getUniqueId());
          if (mallPlayerTabCompleter.getPermissionSet().getPower() >= needPermissionSet
              .getPower()) {
            return completer.onTabComplete(sender, this, alias, args);
          } else {
            return null;
          }
        } else {
          return null;
        }
      } else {
        return null;
      }
    } else {
      return null;
    }
  }


  public void setExecutor(CommandExecutor exe) {
    this.exe = exe;
  }

  public void setTabCompleter(TabCompleter completer) {
    this.completer = completer;
  }

  public void setNeedPermissionSet(PermissionSet needPermissionSet) {
    this.needPermissionSet = needPermissionSet;
  }

  public void setSenderType(SenderType senderType) {
    this.senderType = senderType;
  }

  public void setCommandName(String commandName) {
    this.commandName = commandName;
  }

  public enum SenderType {
    CONSOLE,
    PLAYER,
    BOTH
  }
}
