package info.mallmc.framework.util.helpers;

import info.mallmc.core.api.logs.LL;
import info.mallmc.core.api.logs.Log;
import info.mallmc.core.api.logs.Trigger;
import info.mallmc.framework.Framework;
import info.mallmc.framework.util.Messaging;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ServerSending {

  public static void sendToServer(String server) {
    for (Player p : Bukkit.getOnlinePlayers()) {
      sendToServer(p, server);
    }
  }

  public static void sendToServer(Player player, String server) {
    ByteArrayOutputStream b = new ByteArrayOutputStream();
    DataOutputStream out = new DataOutputStream(b);
    try {
      out.writeUTF("Connect");
      out.writeUTF(server);
    } catch (IOException e) {
      Log.error(LL.ERROR, Framework.getInstance().getCurrentServer().getNickname(), Trigger.MESSAGE.name(),"Error sending player to server " + server,
          "Error sending player to server " + server);
      Messaging.sendMessage(player, "&cCould not send you to {0}", server);
      Framework.getInstance().getLogger().info(e.getMessage());
    }
    player.sendPluginMessage(Framework.getInstance(), "BungeeCord", b.toByteArray());
  }
}