package info.mallmc.framework.util.helpers;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class MathHelper {

    public static int randomInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    public static boolean isDivsableByX(int number, int x) {
        if (number % x == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static String getTimeString(Long time) {
        if (time == -1
                || time == 0) {
            return "--:--:--";
        }
        String timeString = String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(time),
                TimeUnit.MILLISECONDS.toSeconds(time) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time)),
                time -
                        TimeUnit.MINUTES.toMillis(TimeUnit.MILLISECONDS.toMinutes(time)) -
                        TimeUnit.SECONDS.toMillis(TimeUnit.MILLISECONDS.toSeconds(time) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time))));

        if (timeString.split(":")[2].length() > 2) {
            return timeString.substring(0, timeString.length() - 1);
        }
        return timeString;
    }

    public static String conventIntToTime(int time) {
        int min = time / 60;
        int sec = time - (min * 60);
        String secLeft = "" + sec;
        if (getIntlength(sec) == 1) {
            secLeft = "0" + sec;
        }
        if (getIntlength(sec) == 0) {
            secLeft = "00";
        }
        return "" + min + ":" + secLeft;
    }
    public static String conventLongToTime(Long time) {
        Long min = time / 60;
        Long sec = time - (min * 60);
        String secLeft = "" + sec;
        if (getLongLength(sec) == 1) {
            secLeft = "0" + sec;
        }
        if (getLongLength(sec) == 0) {
            secLeft = "00";
        }
        return "" + min + ":" + secLeft;
    }


    public static int getIntlength(int length) {
        return String.valueOf(length).length();
    }
    public static Long getLongLength(Long length) {
        return (long) String.valueOf(length).length();
    }
}
