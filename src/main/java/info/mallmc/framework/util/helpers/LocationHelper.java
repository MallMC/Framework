package info.mallmc.framework.util.helpers;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

public class LocationHelper
{
  public static Location getDeserializedLocation(World world, String s) {
    String[] parts = s.split(";");
    double x = Double.parseDouble(parts[0]);
    double y = Double.parseDouble(parts[1]);
    double z = Double.parseDouble(parts[2]);
    try {
      float xx = Float.parseFloat(parts[3]);
      float yy = Float.parseFloat(parts[4]);
      Location location = new Location(world, x, y, z, xx, yy);
      return location;
    } catch (Exception e) {

    }
    return new Location(world, x, y, z);
  }

  public static List<Location> getDeserializedLocations(World world, List<String> s1) {
    List<Location> locations = new ArrayList<>();
    for (String s : s1) {

      String[] parts = s.split(";");
      double x = Double.parseDouble(parts[0]);
      double y = Double.parseDouble(parts[1]);
      double z = Double.parseDouble(parts[2]);
      try {
        float xx = Float.parseFloat(parts[3]);
        float yy = Float.parseFloat(parts[4]);
        locations.add(new Location(world, x, y, z, xx, yy));
      } catch (Exception e) {

      }
      locations.add(new Location(world, x, y, z));
    }
    return locations;
  }

  public static List<Block> blocksFromTwoPoints(Location loc1, Location loc2)
  {
    List<Block> blocks = new ArrayList<Block>();

    int topBlockX = (loc1.getBlockX() < loc2.getBlockX() ? loc2.getBlockX() : loc1.getBlockX());
    int bottomBlockX = (loc1.getBlockX() > loc2.getBlockX() ? loc2.getBlockX() : loc1.getBlockX());

    int topBlockY = (loc1.getBlockY() < loc2.getBlockY() ? loc2.getBlockY() : loc1.getBlockY());
    int bottomBlockY = (loc1.getBlockY() > loc2.getBlockY() ? loc2.getBlockY() : loc1.getBlockY());

    int topBlockZ = (loc1.getBlockZ() < loc2.getBlockZ() ? loc2.getBlockZ() : loc1.getBlockZ());
    int bottomBlockZ = (loc1.getBlockZ() > loc2.getBlockZ() ? loc2.getBlockZ() : loc1.getBlockZ());

    for(int x = bottomBlockX; x <= topBlockX; x++)
    {
      for(int z = bottomBlockZ; z <= topBlockZ; z++)
      {
        for(int y = bottomBlockY; y <= topBlockY; y++)
        {
          Block block = loc1.getWorld().getBlockAt(x, y, z);

          blocks.add(block);
        }
      }
    }

    return blocks;
  }

}
