package info.mallmc.framework.util.helpers;

import com.google.common.collect.ImmutableList;
import info.mallmc.core.api.player.MallPlayer;
import java.util.ArrayList;
import java.util.List;

public class TabCompleteHelper {


  public static List<String> getWhitelist() {
    return ImmutableList.of("on", "off", "setrank", "add", "remove", "list", "getrank");
  }

  public static List<String> getRanks() {
    return ImmutableList.of("Default", "Local", "Loyalty", "Contractor", "Jnr_Security", "Security", "Tester", "Developer");
  }

  public static List<String> getPlayers() {
    List<String> s = new ArrayList<>();
    for (MallPlayer listMallPlayer : MallPlayer.getPlayers().values()) {
      s.add(listMallPlayer.getUsername());
    }
    return s;
  }

  public static List<String> getDirections() {
    return ImmutableList.of("North", "South", "East", "West", "NorthEast", "SouthEast", "NorthWest", "SouthWest");
  }

}
