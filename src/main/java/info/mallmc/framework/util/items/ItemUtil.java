package info.mallmc.framework.util.items;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import info.mallmc.framework.util.Messaging;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;

public class ItemUtil {

    public static ItemStack createItem(Material material) {
        return new ItemStack(material);
    }

    public static ItemStack createItem(Material material, String name) {
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(Messaging.colorizeMessage(name));
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack createItemWithDamage(Material material, byte damage, String name) {
        ItemStack item = new ItemStack(material, 1, damage);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(Messaging.colorizeMessage(name));
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack createItem(Material material,byte damage, String name, String... lore) {
        ItemStack item = new ItemStack(material, 1 , damage);
        ItemMeta meta = item.getItemMeta();
        List<String> loreList = new ArrayList<>();
        for (String loreValue : lore) {
            loreList.add(Messaging.colorizeMessage(loreValue));
        }
        meta.setDisplayName(Messaging.colorizeMessage(name));
        meta.setLore(loreList);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack createItem(Material material,byte damage, String name, List<String> lore) {
        ItemStack item = new ItemStack(material, 1 , damage);
        ItemMeta meta = item.getItemMeta();
        List<String> loreList = new ArrayList<>();
        for (String loreValue : lore) {
            loreList.add(Messaging.colorizeMessage(loreValue));
        }
        meta.setDisplayName(Messaging.colorizeMessage(name));
        meta.setLore(loreList);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack createItem(Material material, String name, String... lore) {
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();
        List<String> loreList = new ArrayList<>();
        for (String loreValue : lore) {
            loreList.add(Messaging.colorizeMessage(loreValue));
        }
        meta.setDisplayName(Messaging.colorizeMessage(name));
        meta.setLore(loreList);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack createItem(Material material, String name, List<String> lore) {
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();
        List<String> loreList = new ArrayList<>();
        for (String loreValue : lore) {
            loreList.add(Messaging.colorizeMessage(loreValue));
        }
        meta.setDisplayName(Messaging.colorizeMessage(name));
        meta.setLore(loreList);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack createStack(Material material, int amount) {
        return new ItemStack(material, amount);
    }

    public static ItemStack createStack(Material material, int amount, String name) {
        ItemStack item = new ItemStack(material, amount);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(Messaging.colorizeMessage(name));
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack createStack(Material material, int amount, String name,
        String... lore) {
        ItemStack item = new ItemStack(material, amount);
        ItemMeta meta = item.getItemMeta();

        List<String> lores = new ArrayList<>();
        for (String aLore : lore) {
            lores.add(Messaging.colorizeMessage(aLore));
        }
        meta.setDisplayName(Messaging.colorizeMessage(name));
        meta.setLore(lores);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack createStack(Material material, int amount, String name,
        List<String> lore) {
        ItemStack item = new ItemStack(material, amount);
        ItemMeta meta = item.getItemMeta();

        List<String> lores = new ArrayList<>();
        for (String aLore : lore) {
            lores.add(Messaging.colorizeMessage(aLore));
        }
        meta.setDisplayName(Messaging.colorizeMessage(name));
        meta.setLore(lores);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack createPotion(List<PotionEffect> type) {
        ItemStack itemStack = new ItemStack(Material.POTION);
        PotionMeta potionMeta = (PotionMeta) itemStack.getItemMeta();
        for (PotionEffect potionEffect : type) {
            potionMeta.addCustomEffect(potionEffect, false);
        }
        itemStack.setItemMeta(potionMeta);
        return itemStack;
    }

    public static ItemStack getSkull(String textureID, String name, List<String> lore) {
        ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);

        SkullMeta headMeta = (SkullMeta) head.getItemMeta();
        headMeta.setDisplayName(Messaging.colorizeMessage(name));
        headMeta.setLore(lore);
        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        profile.getProperties().put("textures", new Property("textures", textureID));
        Field profileField = null;
        try {
            profileField = headMeta.getClass().getDeclaredField("profile");
            profileField.setAccessible(true);
            profileField.set(headMeta, profile);
        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
            e1.printStackTrace();
        }
        head.setItemMeta(headMeta);
        return head;
    }

    public static ItemStack getSkullPlayer(Player player, String name, List<String> lore) {
        ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        SkullMeta headMeta = (SkullMeta) head.getItemMeta();
        headMeta.setDisplayName(Messaging.colorizeMessage(name));
        headMeta.setLore(lore);
        headMeta.setOwner(player.getName());
        head.setItemMeta(headMeta);
        return head;
    }
}
