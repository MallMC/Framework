package info.mallmc.framework.util.items;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class FrameworkItems {

  public static ItemStack backToHub() {
    return ItemUtil.createItem(Material.EYE_OF_ENDER, "&bBack to Hub &7&o(Right Click)");
  }

}
