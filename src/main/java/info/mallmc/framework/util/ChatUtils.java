package info.mallmc.framework.util;

import info.mallmc.core.api.GameState;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.core.api.player.Rank;
import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class ChatUtils {

  public static ArrayList<String> bannedWords = new ArrayList<>();

  /**
   * Checks if the given message contains any profanity.
   * @param mallPlayer  The player who sent the message
   * @param message     The message to be checked
   * @return            Whether or not the message contains profanity
   */
  public static boolean hasProfanity(MallPlayer mallPlayer, String message){
    if(mallPlayer.getPermissionSet().getPower() >= PermissionSet.SNR_STAFF.getPower()){
      return false;
    }

    for(String msgs : message.split(" ")){
      if(bannedWords.contains(msgs.toLowerCase())){
        Messaging.sendMessage(mallPlayer, "framework.chat.profanity");
        return true;
      }
    }
    return false;
  }

  /**
   * Checks whether the whole or half of the message is in capitals
   * @param message   The message to check
   * @return          Whether or not the message is half or wholly capitals.
   */
  public static boolean isInCaps(String message){
    boolean isInCaps = false;
    if(message.length() > 4){
      String replaceAll = message.replaceAll("/[^A-Za-z0-9 ]/", "");
      int capitalCount = 0;
      for(char character : replaceAll.toCharArray()){
        if(Character.isUpperCase(character)){
          capitalCount++;
        }
      }
      isInCaps = replaceAll.length() / 2 < capitalCount;
    }
    return isInCaps;
  }

  /**
   * Checks whether or not the player is spamming
   * @param player    The player to check
   * @param message   The message sent to check
   * @return          Whether or not the player is spamming.
   */
  public static boolean isSpamming(MallPlayer player, String message){
    if(player.getPermissionSet().getPower() >= PermissionSet.SNR_STAFF.getPower()){
      return false;
    }
    int repetitionCount = 0;
    for (String recentMessage : player.getRecentMessages()){
      if(recentMessage.contains(message) || recentMessage == message){
          repetitionCount++;
      }
    }
    return repetitionCount > 2;
  }

  /**
   * Checks if a player is mentioned in the message
   * @param mallPlayer    The player who sent the message
   * @param message
   * @return
   */
  public static boolean isMentioned(MallPlayer mallPlayer, String message){
    if(message.startsWith("@") && message.length() > 1){
      if(mallPlayer.getPermissionSet().getPower() < PermissionSet.SPECIAL.getPower()){
        Messaging.sendMessage(mallPlayer, "framework.chat.pingvip");
        return false;
      }
      String userName = message.substring(1, message.lastIndexOf(message.split(" ")[0]));
      Player playerToPing = Bukkit.getServer().getPlayer(userName);
      if(playerToPing == null){
        Messaging.sendMessage(mallPlayer, "global.command.playernotfound");
        return false;
      }
      if(playerToPing.getUniqueId() == mallPlayer.getUuid()){
        Messaging.sendMessage(mallPlayer, "framework.chat.pingself");
        return false;
      }
      MallPlayer mallPlayer1 = MallPlayer.getPlayer(playerToPing.getUniqueId());

      if(mallPlayer.getPermissionSet().getPower() < mallPlayer1.getPermissionSet().getPower() && mallPlayer1.getPermissionSet().getPower() > PermissionSet.SNR_STAFF.getPower()){
        Messaging.sendMessage(mallPlayer, "framework.chat.pingabove");
        return false;
      }
      Messaging.sendMessage(mallPlayer, "framework.chat.pinger",
          mallPlayer1.getRank().getColor(), mallPlayer.getUsername());

      Messaging.sendMessage(mallPlayer1, "framework.chat.pinged",
          mallPlayer.getRank().getColor(), mallPlayer.getUsername());
      Player pinger = Bukkit.getServer().getPlayer(mallPlayer.getUuid());
      pinger.playSound(pinger.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
      return true;
    }
    return false;
  }

  public static void sendMessage(MallPlayer mallPlayer, String message){
    if(mallPlayer.isChatMuted()){
      Messaging.sendMessage(mallPlayer, "framework.chat.muted");
    }
    ChatColor color = ChatColor.WHITE;
    if (mallPlayer.getRank().getPower() == Rank.DEFAULT.getPower()) {
      color = ChatColor.GRAY;
    }
    String coloredMessage = Messaging.colorizeMessage(
        mallPlayer.getRankPrefix() + mallPlayer.getUsername() + "&8: " + color) + message + " ";

    if (GameState.getGameState() == GameState.IN_GAME && mallPlayer.isInSpecChat()) {
      for (Player ps : Bukkit.getOnlinePlayers()) {
        MallPlayer mps = MallPlayer.getPlayer(ps.getUniqueId());
        if (mps.isInSpecChat()) {
          ps.sendMessage(coloredMessage);
        }
      }
    } else {
      for (Player ps : Bukkit.getOnlinePlayers()) {
        ps.sendMessage(coloredMessage);
      }
    }

    Bukkit.getConsoleSender().sendMessage(coloredMessage);
    mallPlayer.addRecentMessage(coloredMessage);
  }
}
