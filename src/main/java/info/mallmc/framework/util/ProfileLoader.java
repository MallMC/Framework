package info.mallmc.framework.util;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import info.mallmc.core.MallCore;
import info.mallmc.core.api.logs.LL;
import info.mallmc.core.api.logs.Log;
import info.mallmc.framework.Framework;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class ProfileLoader {

    private final String uuid;
    private final String name;
    private final String skinOwner;

    public ProfileLoader(String uuid, String name) {
        this(uuid, name, name);
    }

    public ProfileLoader(String uuid, String name, String skinOwner) {
        this.uuid = uuid == null ? null : uuid.replaceAll("-", ""); //We add these later
        String displayName = ChatColor.translateAlternateColorCodes('&', name);
        this.name = ChatColor.stripColor(displayName);
        this.skinOwner = skinOwner;
    }

    public GameProfile loadProfile() {
        UUID id = uuid == null ? parseUUID(getUUID(name)) : parseUUID(uuid);
        GameProfile profile = new GameProfile(id, name);
        addProperties(profile);
        return profile;
    }

    private void addProperties(GameProfile profile) {
        if (MallCore.getInstance().getRedis().isInCache("properties-" + skinOwner)) {
            String json = (String) MallCore.getInstance().getRedis()
                    .getFromCache("properties-" + skinOwner, String.class);
            for (Property prop :
                    getPropertyFromJSON(json)) {
                profile.getProperties().put(prop.getName(), prop);
            }
            return;
        }
        try {
            // Get the name from SwordPVP
            URL url = new URL("https://mcapi.de/api/user/" + skinOwner + "?unsigned=false");
            URLConnection uc = url.openConnection();
            uc.addRequestProperty("User-Agent", "Mozilla/5.0");

            // Parse it
            String json = new Scanner(uc.getInputStream(), "UTF-8").useDelimiter("\\A").next();
            MallCore.getInstance().getRedis().putInCache("properties-" + skinOwner, json, String.class);
            for (Property prop :
                    getPropertyFromJSON(json)) {
                profile.getProperties().put(prop.getName(), prop);
            }
        } catch (Exception e) {
            Log.error(LL.ERROR, Framework.getInstance().getCurrentServer().getNickname(), "Loading", "Error grabbing the date for the profiles", e.getMessage());
        }

    }

    @SuppressWarnings("deprecation")
    private String getUUID(String name) {
        return Bukkit.getOfflinePlayer(name).getUniqueId().toString().replaceAll("-", "");
    }

    private List<Property> getPropertyFromJSON(String json) {
        List<Property> propertyList = new ArrayList<>();
        try {
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(json);
            JSONObject properties = (JSONObject) obj.get("properties");
            JSONArray raw = (JSONArray) properties.get("raw");
            for (int i = 0; i < raw.size(); i++) {
                try {
                    JSONObject property = (JSONObject) raw.get(i);
                    String name = (String) property.get("name");
                    String value = (String) property.get("value");
                    String signature =
                            property.containsKey("signature") ? (String) property.get("signature") : null;
                    if (signature != null) {
                        propertyList.add(new Property(name, value, signature));
                    } else {
                        propertyList.add(new Property(value, name));
                    }
                } catch (Exception e) {
                    Log.error(LL.ERROR,Framework.getInstance().getCurrentServer().getNickname(), "Failed to apply auth property ", "Failed to apply auth property", e.getMessage());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.error(LL.ERROR,Framework.getInstance().getCurrentServer().getNickname(), "Loading ", "Error grabbing the data for Profiles", e.getMessage());
        }
        return propertyList;
    }


    private UUID parseUUID(String uuidStr) {
        // Split uuid in to 5 components
        String[] uuidComponents = new String[]{uuidStr.substring(0, 8),
                uuidStr.substring(8, 12), uuidStr.substring(12, 16),
                uuidStr.substring(16, 20),
                uuidStr.substring(20, uuidStr.length())
        };

        // Combine components with a dash
        StringBuilder builder = new StringBuilder();
        for (String component : uuidComponents) {
            builder.append(component).append('-');
        }

        // Correct uuid length, remove last dash
        builder.setLength(builder.length() - 1);
        return UUID.fromString(builder.toString());
    }
}
