package info.mallmc.framework.util.registies;

import info.mallmc.framework.api.MallInventory;
import info.mallmc.framework.entities.EntityTypes;
import info.mallmc.framework.entities.VillagerNPC;
import info.mallmc.framework.util.Messaging;
import java.util.HashMap;
import java.util.function.Consumer;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEntityEvent;

public class EntityRegistry
{
  private static HashMap<String, Consumer<PlayerInteractEntityEvent>> villagers = new HashMap<>();

  public static void registerInteraction(String itemName,
      Consumer<PlayerInteractEntityEvent> eventConsumer) {
    villagers.put(itemName, eventConsumer);
  }


  public static void handleEvent(Player player, String name, PlayerInteractEntityEvent event) {
    name = ChatColor.stripColor(name);
    if (ChatColor.stripColor(name).contains("Busy")) {
      Messaging.sendRawMessage(player, "global.busy", event.getRightClicked().getName());
      event.setCancelled(true);
      return;
    } else if (villagers.containsKey(ChatColor.stripColor(name))) {
      Consumer<PlayerInteractEntityEvent> entityToDo = villagers.get(ChatColor.stripColor(name));
      entityToDo.accept(event);
      event.setCancelled(true);
    }
  }

  public static VillagerNPC registerVillager(Location location, String name) {
    VillagerNPC npc = (VillagerNPC) EntityTypes
        .spawnEntity(
            new VillagerNPC(((CraftWorld) location.getWorld()).getHandle(), location, name),
            location,
            name);
    return npc;
  }

  public static VillagerNPC registerNPCWithInventory(Location location, String name,
      MallInventory inventory) {
    VillagerNPC npc = registerVillager(location, name);
    name = Messaging.colorizeMessage(name);
    name = ChatColor.stripColor(name);
    registerInteraction(name, event -> inventory.open(event.getPlayer()));
    return npc;
  }

}
