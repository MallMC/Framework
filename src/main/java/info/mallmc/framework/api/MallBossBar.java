package info.mallmc.framework.api;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.util.Messaging;
import java.util.List;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

public class MallBossBar
{
  private BossBar bossBar;
  private List<UUID> players;
  public MallBossBar(String title, BarColor color, BarStyle style)
  {
    bossBar = Bukkit.createBossBar(Messaging.colorizeMessage(title), color, style);
  }

  public void addPlayer(MallPlayer mallPlayer)
  {
    try {
      bossBar.addPlayer(Bukkit.getPlayer(mallPlayer.getUuid()));
      players.add(mallPlayer.getUuid());
    } catch (Exception e) {}
  }

  public void removePlayer(MallPlayer mallPlayer)
  {
    try{
      bossBar.removePlayer(Bukkit.getPlayer(mallPlayer.getUuid()));
      players.remove(mallPlayer.getUuid());
    }catch (Exception e) {}
  }

  public void setTitle(String title)
  {
    bossBar.setTitle(Messaging.colorizeMessage(title));
  }

  public void setVisible(boolean visable)
  {
    bossBar.setVisible(true);
  }

  public void setColor(BarColor color) { bossBar.setColor(color);}

  public void setStyle(BarStyle style) { bossBar.setStyle(style);}

  public List<Player> getPlayers()
  {
    return bossBar.getPlayers();
  }
}