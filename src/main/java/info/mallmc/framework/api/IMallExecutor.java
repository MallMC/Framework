package info.mallmc.framework.api;

import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.framework.util.MallCommand.SenderType;
import java.util.List;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

public interface IMallExecutor extends TabCompleter, CommandExecutor {

  boolean onCommand(CommandSender commandSender, Command command, String aliasUsed, String[] args);

  default List<String> onTabComplete(CommandSender commandSender, Command command, String s,
      String[] args) {
    return null;
  }

  default PermissionSet getNeedPermissionSet() {
    return PermissionSet.ALL;
  }

  default SenderType getSenderType() {
    return SenderType.PLAYER;
  }
}
