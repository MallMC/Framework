package info.mallmc.framework.api;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.Framework;
import info.mallmc.framework.interactions.Interaction;
import info.mallmc.framework.interactions.InteractionRegistry;
import info.mallmc.framework.util.Messaging;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public abstract class MallInventory {

    protected InventoryProxy inventory;
    private Inventory bukkitInventory;
    private boolean populated = false;
    private BukkitRunnable updaterTask;


    public MallInventory(MallPlayer mallPlayer, String nameKey, int size){
        this.bukkitInventory = Bukkit.createInventory(null, getInvSizeForCount(size),
            Messaging.getMessage(mallPlayer.getLanguageIdentifier(), nameKey));
        this.inventory = new InventoryProxy(bukkitInventory, Bukkit.createInventory(null, getInvSizeForCount(size), Messaging.getMessage(mallPlayer.getLanguageIdentifier(), nameKey)));
        InteractionRegistry.registerInteraction(Interaction.CLICK_INSIDE_INVENTORY,
            this::handleClickEvent);
        InteractionRegistry.registerInteraction(Interaction.CLOSE_INVENTORY, this::handleCloseEvent);
        InteractionRegistry.registerInteraction(Interaction.DRAG_INVENTORY, this::handleInventoryDrag);
    }

  public MallInventory(MallPlayer mallPlayer, String nameKey, InventoryType type){
    this.bukkitInventory = Bukkit.createInventory(null, type,
        Messaging.getMessage(mallPlayer.getLanguageIdentifier(), nameKey));
    this.inventory = new InventoryProxy(bukkitInventory, Bukkit.createInventory(null, type, Messaging.getMessage(mallPlayer.getLanguageIdentifier(), nameKey)));
    InteractionRegistry.registerInteraction(Interaction.CLICK_INSIDE_INVENTORY,
        this::handleClickEvent);
    InteractionRegistry.registerInteraction(Interaction.CLOSE_INVENTORY, this::handleCloseEvent);
    InteractionRegistry.registerInteraction(Interaction.DRAG_INVENTORY, this::handleInventoryDrag);
  }

    public final void open(Player p) {
        try {
            if (!this.populated) {
                this.populate();
                this.inventory.apply();
                this.populated = true;
            }
            this.openInventory(p);
        } catch (Throwable e) {
            this.throwError(e);
        }
    }

    public String getMessage(MallPlayer mallPlayer, String keyName){
        return Messaging.colorizeMessage(Messaging.getMessage(mallPlayer.getLanguageIdentifier(),keyName));
    }

    public List<String> getLore(MallPlayer mallPlayer, String keyName){
        List<String> strings =  MallCore.getInstance().getLanguageMap().get(mallPlayer.getLanguageIdentifier()).getStrings(keyName);
        for(String string : strings){
            strings.set(strings.indexOf(string), Messaging.colorizeMessage(string));
        }
        return strings;
    }


    protected abstract void populate();

    protected final void repopulate() {
        try {
            this.inventory.clear();
            this.populate();
            this.inventory.apply();
            this.populated = true;
        } catch (Throwable e) {
            this.throwError(e);
        }
    }

    protected final void setUpdateTicks(int ticks, boolean sync) {
        if (this.updaterTask != null) {
            this.updaterTask.cancel();
            this.updaterTask = null;
        }
        this.updaterTask = new GUIUpdateTask(this);
        if (sync) {
            this.updaterTask.runTaskTimer(Framework.getInstance(), 0, ticks);
        } else {
            this.updaterTask.runTaskTimerAsynchronously(Framework.getInstance(), 0, ticks);
        }
    }

    private void throwError(Throwable e) {
        e.printStackTrace();
    }


    public final void close() {
        new ArrayList<>(this.getInventory().getViewers()).forEach(HumanEntity::closeInventory);
    }

    protected void openInventory(Player p) {
        p.openInventory(this.getInventory());
    }

    protected Inventory getInventory() {
        return bukkitInventory;
    }

    protected abstract void onPlayerClick(InventoryClickEvent event);

    protected void onPlayerCloseInv() {
    }

    protected void onPlayerDrag(InventoryDragEvent event) {
    }

    protected void onTickUpdate() {
    }


    private void handleInventoryDrag(Event event){
        InventoryDragEvent inventoryCloseEvent = (InventoryDragEvent) event;
        if(inventoryCloseEvent.getInventory() == null || !inventoryCloseEvent.getInventory().equals(bukkitInventory))
            return;
        if(!(inventoryCloseEvent.getWhoClicked() instanceof Player)) return;
        inventoryCloseEvent.setCancelled(true);
        onPlayerDrag(inventoryCloseEvent);
    }

    private void handleCloseEvent(Event event){
        InventoryCloseEvent inventoryCloseEvent = (InventoryCloseEvent) event;
        if(inventoryCloseEvent.getInventory() == null || !inventoryCloseEvent.getInventory().equals(bukkitInventory))
            return;
        if(!(inventoryCloseEvent.getPlayer() instanceof Player)) return;
        onPlayerCloseInv();
    }

    private void handleClickEvent(Event event){
        InventoryClickEvent inventoryClickEvent = (InventoryClickEvent) event;
        if(this.bukkitInventory.getViewers().contains(inventoryClickEvent.getWhoClicked())){
            List<InventoryAction> deniedActions = new ArrayList<>(Arrays.asList(
                InventoryAction.CLONE_STACK,
                InventoryAction.COLLECT_TO_CURSOR,
                InventoryAction.UNKNOWN,
                InventoryAction.MOVE_TO_OTHER_INVENTORY
            ));
            if (deniedActions.contains(inventoryClickEvent.getAction())) {
                inventoryClickEvent.setCancelled(true);
            }
        }
        if(inventoryClickEvent.getInventory() == null || !inventoryClickEvent.getInventory().equals(bukkitInventory))
            return;
        inventoryClickEvent.setCancelled(true);
        if(!(inventoryClickEvent.getWhoClicked() instanceof Player)) return;
        onPlayerClick(inventoryClickEvent);
    }

    protected final int getInvSizeForCount(int count) {
        int size = (count / 9) * 9;
        if (count % 9 > 0) size += 9;
        if (size < 9) return 9;
        if (size > 54) return 54;
        return size;
    }

    private class GUIUpdateTask extends BukkitRunnable {

        private MallInventory gui;

        public GUIUpdateTask(MallInventory gui) {
            this.gui = gui;
        }

        public void run() {
            try {
                this.gui.repopulate();
                this.gui.onTickUpdate();
            } catch (Throwable e) {
                this.gui.throwError(e);
            }
        }
    }

    public class InventoryProxy implements Inventory {

        private Inventory mainInventory;
        private Inventory proxyInventory;

        private InventoryProxy(Inventory mainInventory, Inventory proxyInventory) {
            this.mainInventory = mainInventory;
            this.proxyInventory = proxyInventory;
        }

        public void apply() {
            this.mainInventory.setContents(this.proxyInventory.getContents());
        }

        public int getSize() {
            return proxyInventory.getSize();
        }

        public int getMaxStackSize() {
            return proxyInventory.getMaxStackSize();
        }

        public void setMaxStackSize(int i) {
            proxyInventory.setMaxStackSize(i);
        }

        public String getName() {
            return proxyInventory.getName();
        }

        public ItemStack getItem(int i) {
            return proxyInventory.getItem(i);
        }

        public void setItem(int i, ItemStack itemStack) {
            proxyInventory.setItem(i, itemStack);
        }

        public HashMap<Integer, ItemStack> addItem(ItemStack... itemStacks) throws IllegalArgumentException {
            return proxyInventory.addItem(itemStacks);
        }

        public HashMap<Integer, ItemStack> removeItem(ItemStack... itemStacks) throws IllegalArgumentException {
            return proxyInventory.removeItem(itemStacks);
        }

        public ItemStack[] getContents() {
            return proxyInventory.getContents();
        }

        public void setContents(ItemStack[] itemStacks) throws IllegalArgumentException {
            proxyInventory.setContents(itemStacks);
        }

        public ItemStack[] getStorageContents() {
            return new ItemStack[0];
        }

        public void setStorageContents(ItemStack[] itemStacks) throws IllegalArgumentException {

        }

        public boolean contains(int i) {
            return proxyInventory.contains(i);
        }

        public boolean contains(Material material) throws IllegalArgumentException {
            return proxyInventory.contains(material);
        }

        public boolean contains(ItemStack itemStack) {
            return proxyInventory.contains(itemStack);
        }

        public boolean contains(int i, int i1) {
            return proxyInventory.contains(i, i1);
        }

        public boolean contains(Material material, int i) throws IllegalArgumentException {
            return proxyInventory.contains(material, i);
        }

        public boolean contains(ItemStack itemStack, int i) {
            return proxyInventory.contains(itemStack, i);
        }

        public boolean containsAtLeast(ItemStack itemStack, int i) {
            return proxyInventory.containsAtLeast(itemStack, i);
        }

        public HashMap<Integer, ? extends ItemStack> all(int i) {
            return proxyInventory.all(i);
        }

        public HashMap<Integer, ? extends ItemStack> all(Material material) throws IllegalArgumentException {
            return proxyInventory.all(material);
        }

        public HashMap<Integer, ? extends ItemStack> all(ItemStack itemStack) {
            return proxyInventory.all(itemStack);
        }

        public int first(int i) {
            return proxyInventory.first(i);
        }

        public int first(Material material) throws IllegalArgumentException {
            return proxyInventory.first(material);
        }

        public int first(ItemStack itemStack) {
            return proxyInventory.first(itemStack);
        }

        public int firstEmpty() {
            return proxyInventory.firstEmpty();
        }

        public void remove(int i) {
            proxyInventory.remove(i);
        }

        public void remove(Material material) throws IllegalArgumentException {
            proxyInventory.remove(material);
        }

        public void remove(ItemStack itemStack) {
            proxyInventory.remove(itemStack);
        }

        public void clear(int i) {
            proxyInventory.clear(i);
        }

        public void clear() {
            proxyInventory.clear();
        }

        public List<HumanEntity> getViewers() {
            return mainInventory.getViewers();
        }

        public String getTitle() {
            return mainInventory.getTitle();
        }

        public InventoryType getType() {
            return mainInventory.getType();
        }

        public InventoryHolder getHolder() {
            return mainInventory.getHolder();
        }

        public ListIterator<ItemStack> iterator() {
            return proxyInventory.iterator();
        }

        public ListIterator<ItemStack> iterator(int i) {
            return proxyInventory.iterator(i);
        }

        @Override
        public Location getLocation() {
            return new Location(Framework.getInstance().getServer().getWorld("world"), 0, 0, 0);
        }

    }
}
