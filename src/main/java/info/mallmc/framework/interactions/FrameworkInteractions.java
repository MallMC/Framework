package info.mallmc.framework.interactions;

import info.mallmc.framework.util.helpers.ServerSending;
import info.mallmc.framework.util.items.FrameworkItems;
import info.mallmc.framework.util.registies.EntityRegistry;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftVillager;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class FrameworkInteractions
{


  public static void registerInteractions() {
    InteractionRegistry
        .registerInteraction(Interaction.RIGHT_CLICK, FrameworkInteractions::returnToHub);
    InteractionRegistry
        .registerInteraction(Interaction.CLICK_INSIDE_INVENTORY, FrameworkInteractions::handleInventory);
    InteractionRegistry.registerInteraction(Interaction.RIGHT_CLICK_ENTITY,
            FrameworkInteractions::handleNPCInventoryClick);
  }

  private static void returnToHub(Event event) {
    PlayerInteractEvent e = (PlayerInteractEvent) event;
    if (e.getItem() != null) {
      Player p = e.getPlayer();
      if (p.getInventory().getItemInMainHand().equals(FrameworkItems.backToHub())) {
        ServerSending.sendToServer(e.getPlayer(), "hub1");
      }
    }
  }


  private static void handleInventory(Event event) {
    InventoryClickEvent inventoryClickEvent = (InventoryClickEvent) event;
    if (inventoryClickEvent.getClickedInventory() == null) {
      return;
    }
  }

  private static void handleNPCInventoryClick(Event e) {
    PlayerInteractEntityEvent event = (PlayerInteractEntityEvent) e;
    if (event.getRightClicked() instanceof CraftVillager) {
      CraftVillager npc = (CraftVillager) event.getRightClicked();
      EntityRegistry.handleEvent(event.getPlayer(), npc.getName(), event);
    }
  }
}
