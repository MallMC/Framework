package info.mallmc.framework.commands.util;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.framework.api.IMallExecutor;
import info.mallmc.framework.util.MallCommand.SenderType;
import info.mallmc.framework.util.Messaging;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DebugCommand implements IMallExecutor {

  @Override
  public boolean onCommand(CommandSender commandSender, Command command, String aliasUsed,
      String[] args) {
    Player player = (Player) commandSender;
    MallPlayer mallPlayer = MallPlayer.getPlayer(player.getUniqueId());
    mallPlayer.setDebugMode(!mallPlayer.isDebugMode());
    ChatColor chatColor = mallPlayer.isDebugMode() ? ChatColor.GREEN : ChatColor.RED;
    String message = mallPlayer.isDebugMode() ? "enabled" : "disabled";
    Messaging.sendMessage(commandSender, "framework.command.debug." + message, chatColor);
    return false;
  }

  @Override
  public PermissionSet getNeedPermissionSet() {
    return PermissionSet.SNR_STAFF;
  }

  @Override
  public SenderType getSenderType() {
    return SenderType.PLAYER;
  }
}
