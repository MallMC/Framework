package info.mallmc.framework.commands.util;

import info.mallmc.core.MallCore;
import info.mallmc.framework.api.IMallExecutor;
import info.mallmc.framework.util.MallCommand.SenderType;
import info.mallmc.framework.util.Messaging;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class ReloadMessagesCommand implements IMallExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
        MallCore.getInstance().getDatabase().setupLanguage();
        Messaging.sendMessage(cs, "framework.language.reloaded");
        return true;
    }

    @Override
    public SenderType getSenderType() {
        return SenderType.BOTH;
    }
}