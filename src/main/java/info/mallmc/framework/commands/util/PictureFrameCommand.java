package info.mallmc.framework.commands.util;

import com.google.common.collect.ImmutableList;
import info.mallmc.framework.api.IMallExecutor;
import info.mallmc.framework.util.Messaging;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.util.StringUtil;

public class PictureFrameCommand implements IMallExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 0 || args.length < 2) {
            Messaging.sendMessage(sender, "framework.command.picture.usage", "/picture create <image>");
            return false;
        }
        if ("create".equalsIgnoreCase(args[0])) {
          //TODO: Maps
//            Framework.getInstance().getMapStuff().startPlacing((Player) sender, args[1], true);
            Messaging.sendMessage(sender, "framework.command.picture.creating", args[1]);
            return true;
        }
        Messaging.sendMessage(sender, "framework.command.picture.usage");
        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String s,
                                      String[] args) {
        if (args.length == 1) {
            return StringUtil.copyPartialMatches(args[1], ImmutableList.of("create"), new ArrayList<>());
        } else {
            return null;
        }
    }
}
