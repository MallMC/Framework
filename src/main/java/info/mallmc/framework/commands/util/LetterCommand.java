package info.mallmc.framework.commands.util;

import info.mallmc.framework.api.IMallExecutor;
import info.mallmc.framework.util.Messaging;
import info.mallmc.framework.util.helpers.TabCompleteHelper;
import info.mallmc.framework.util.letters.Direction;
import info.mallmc.framework.util.letters.Letter;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

public class LetterCommand implements IMallExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 0 || args.length < 2) {
            Messaging
                .sendMessage(sender, "framework.command.letter.usage");
            return false;
        }
        Player playerExecutor = (Player) sender;
        Letter.centreString(args[0], Material.WOOL, DyeColor.BLACK.getWoolData(), playerExecutor.getLocation(),
                Direction.getDirection(args[1]));
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String s, String[] args) {
        return StringUtil.copyPartialMatches(args[1], TabCompleteHelper.getDirections(), new ArrayList<>());
    }

}
