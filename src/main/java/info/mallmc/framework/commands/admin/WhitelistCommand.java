package info.mallmc.framework.commands.admin;

import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.core.api.player.Rank;
import info.mallmc.framework.Framework;
import info.mallmc.framework.api.IMallExecutor;
import info.mallmc.framework.util.FromNameUtil;
import info.mallmc.framework.util.MallCommand.SenderType;
import info.mallmc.framework.util.Messaging;
import info.mallmc.framework.util.helpers.TabCompleteHelper;
import info.mallmc.framework.util.helpers.UUIDHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.util.StringUtil;

public class WhitelistCommand implements IMallExecutor {

  @Override
  public boolean onCommand(CommandSender commandSender, Command command, String aliasUsed,
      String[] args) {
    if (args.length == 0) {
      Messaging.sendMessage(commandSender, "framework.command.whitelist.usage");
      return false;
    }

    switch (args[0].toLowerCase()) {
      case "on": {
        Messaging.sendMessage(commandSender, "framework.command.whitelist.setOn");
        Messaging.broadcastMessage("global.announce.whitelistOn");
        Framework.getInstance().getCurrentServer().setWhitelisted(true);
        break;
      }
      case "off": {
        Messaging.sendMessage(commandSender, "framework.command.whitelist.setOff");
        Messaging.broadcastMessage("global.announce.whitelistOff");
        Framework.getInstance().getCurrentServer().setWhitelisted(false);
        break;
      }
      case "getrank": {
        Messaging.sendMessage(commandSender, "framework.command.whitelist.currentRank", Framework.getInstance().getCurrentServer().getWhitelistLevel().name());
        break;
      }
      case "setrank": {
        if (args.length == 1) {
          Messaging
              .sendMessage(commandSender, "framework.command.whitelist.usage.setrank");
          break;
        }
        Rank rankToSet = Rank.getRankFromName(args[1], Rank.DEVELOPER);
        Framework.getInstance().getCurrentServer().setWhitelistLevel(rankToSet);
        Messaging.sendMessage(commandSender, "framework.command.whitelist.rankSet", rankToSet.getName());
        break;
      }
      case "list": {
        Messaging.sendMessage(commandSender, "framework.command.whitelist.list.title");
        for (UUID uuid : Framework.getInstance().getCurrentServer().getOverride()) {
          Messaging.sendMessage(commandSender, "framework.command.whitelist.list.item", UUIDHelper.getName(uuid));
        }
        break;
      }
      case "add": {
        ArrayList<UUID> st = Framework.getInstance().getCurrentServer().getOverride();
        try {
          st.add(UUIDHelper.getUUID(args[1]));
          Messaging.sendMessage(commandSender, "framework.command.whitelist.added", args[1]);
          Framework.getInstance().getCurrentServer().setOverride(st);
        } catch (Exception e) {
          Messaging.sendMessage(commandSender, "global.command.offlinePlayer");
        }
        break;
      }
      case "remove":
        ArrayList<UUID> st = Framework.getInstance().getCurrentServer().getOverride();
        try {
          st.remove(UUIDHelper.getUUID(args[1]));
          Framework.getInstance().getCurrentServer().setOverride(st);
          Messaging.sendMessage(commandSender, "framework.command.whitelist.removed", args[1]);
        } catch (Exception e) {
          Messaging.sendMessage(commandSender, "global.command.offlinePlayer");
        }


    }
    return false;
  }

  @Override
  public List<String> onTabComplete(CommandSender commandSender, Command command, String s,
      String[] args) {
    if (args.length == 1) {
      return StringUtil
          .copyPartialMatches(args[0], TabCompleteHelper.getWhitelist(), new ArrayList<>());
    } else if (args.length > 1) {
      if (args[0].equalsIgnoreCase("setrank")) {
        return StringUtil
            .copyPartialMatches(args[1], TabCompleteHelper.getRanks(), new ArrayList<>());
      }
      if (args[0].equalsIgnoreCase("add") || args[0].equalsIgnoreCase("remove")) {
        return StringUtil
            .copyPartialMatches(args[1], TabCompleteHelper.getPlayers(), new ArrayList<>());
      }
      if (args[0].equalsIgnoreCase("remove")) {
        List<String> names = new ArrayList<>();
        for (UUID st : Framework.getInstance().getCurrentServer().getOverride()) {
            names.add(UUIDHelper.getName(st));
        }
        return StringUtil.copyPartialMatches(args[1], names, new ArrayList<>());
      }
    } else {
      return null;
    }
    return null;
  }

  @Override
  public PermissionSet getNeedPermissionSet() {
    return PermissionSet.STAFF;
  }

  @Override
  public SenderType getSenderType() {
    return SenderType.BOTH;
  }
}
