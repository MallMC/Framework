package info.mallmc.framework.commands.admin;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.framework.api.IMallExecutor;
import info.mallmc.framework.util.MallCommand.SenderType;
import info.mallmc.framework.util.Messaging;
import info.mallmc.framework.util.helpers.TabCompleteHelper;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

public class SetCurrencyCommand implements IMallExecutor {

  @Override
  public boolean onCommand(CommandSender commandSender, Command command, String aliasUsed,
      String[] args) {
    if (args.length == 0 || args.length < 2) {
        Messaging.sendMessage(commandSender, "framework.command.setcurrency.usage");
        return false;
    }
    Player playerToChangeCurrency = Bukkit.getPlayer(args[0]);
    if (playerToChangeCurrency == null) {
        Messaging.sendMessage(commandSender, "global.command.offlinePlayer");
        return false;
    }
    MallPlayer mallPlayerToChangeCurrency = MallPlayer
        .getPlayer(playerToChangeCurrency.getUniqueId());
    mallPlayerToChangeCurrency.setCurrency(Integer.parseInt(args[1]));
    Messaging.sendMessage(commandSender, "framework.command.setcurrency.set", playerToChangeCurrency.getName(), args[1]);
    return true;
  }

  @Override
  public List<String> onTabComplete(CommandSender commandSender, Command command, String s,
      String[] args) {
    if (args.length == 1) {
      return StringUtil
          .copyPartialMatches(args[0], TabCompleteHelper.getPlayers(), new ArrayList<>());
    } else {
      return null;
    }
  }

  @Override
  public PermissionSet getNeedPermissionSet() {
    return PermissionSet.SNR_STAFF;
  }

  @Override
  public SenderType getSenderType() {
    return SenderType.BOTH;
  }
}
