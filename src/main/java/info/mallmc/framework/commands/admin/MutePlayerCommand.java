package info.mallmc.framework.commands.admin;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.framework.api.IMallExecutor;
import info.mallmc.framework.util.MallCommand.SenderType;
import info.mallmc.framework.util.Messaging;
import info.mallmc.framework.util.helpers.TabCompleteHelper;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

public class MutePlayerCommand implements IMallExecutor {

  @Override
  public boolean onCommand(CommandSender commandSender, Command command, String aliasUsed,
      String[] args) {
    if (args.length < 1) {
        Messaging.sendMessage(commandSender, "framework.command.mute.usage");
        return false;
    }
    Player playerToMute = Bukkit.getPlayer(args[0]);
    if (playerToMute == null) {
        Messaging.sendMessage(commandSender, "global.command.offlinePlayer");
        return false;
    }
    MallPlayer mallPlayerToMute = MallPlayer.getPlayer(playerToMute.getUniqueId());
    mallPlayerToMute.setChatMuted(!mallPlayerToMute.isChatMuted());
    String message = mallPlayerToMute.isChatMuted() ? "muted" : "unmuted";
    Messaging.sendMessage(commandSender, "framework.command.mute." + message, playerToMute.getName());
    return true;
  }

  @Override
  public List<String> onTabComplete(CommandSender commandSender, Command command, String s,
      String[] args) {
    if (args.length == 1) {
      return StringUtil.copyPartialMatches(args[0], TabCompleteHelper.getPlayers(), new ArrayList<>());
    } else {
      return null;
    }
  }

  @Override
  public PermissionSet getNeedPermissionSet() {
    return PermissionSet.JNR_STAFF;
  }

  @Override
  public SenderType getSenderType() {
    return SenderType.BOTH;
  }
}
